import os
import pandas as pd


def agmarknetarrival(inputfile):
    fields = ['State', 'Arrivals (Tonnes)']
    tokens = ['span']

    with open(inputfile, "r") as myfile:
        frame = pd.DataFrame(columns=fields)
        record = {}
        for line in myfile:
            line = line.strip()
            if all(x in line for x in tokens):
                temp = line.strip()
                if 'State' in line or 'id="lTotal"' in line:
                    pointer = 0
                else:
                    pointer = 1
                arrindex = temp.index('>') + 1
                trrindex = temp.index('</span>')
                record[fields[pointer]] = temp[arrindex:trrindex]
                if pointer == 1:
                    frame = frame.append(record, ignore_index=True)
    return frame


finalframe = pd.DataFrame()
for folder in os.listdir('dfiles'):
    _f = folder.index("_")
    rawdate = folder[_f+1:]
    _f = rawdate.index("_")
    year = rawdate[:_f]
    month = rawdate[_f+1:]
    # print(year, month)
    for sfile in os.listdir('dfiles/'+folder):
        _d = sfile.index("_")
        sdate = sfile[_d+1:-4]
        _d = sdate.index("-")
        date = sdate[:_d]
        frame = agmarknetarrival('dfiles/'+folder+'/'+sfile)
        frame['Date'] = date+"/"+month+'/'+year
        frame['Date'] = pd.to_datetime(
            frame['Date'],
            format='%d/%m/%Y'
        )
        frame = frame[['Date', 'State', 'Arrivals (Tonnes)']]
        finalframe = finalframe.append(frame)

finalframe.to_csv('Arrivals.csv', index=False)
