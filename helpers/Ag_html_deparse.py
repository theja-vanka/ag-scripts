import csv
import re
import sys


def agmarknet(inputfile, outputfile):
    fields = ('State', 'Arrivals (Tonnes)')
    tokens = ['tr', 'td', 'table', 'th', 'span', 'div', 'style', 'scope']
    with open(inputfile, "r") as myfile, open(outputfile, 'w', newline='') as fw:
        writer = csv.DictWriter(fw, fields, delimiter=',')
        writer.writeheader()
        record = {}
        pointer = -1
        for line in myfile:
            line = line.strip()
            if 'tr>' in line:
                if record:
                    writer.writerow(record)
                record = {}
                pointer = -1
            elif all(x not in line for x in tokens):
                pointer += 1
                record[fields[pointer]] = line.strip()
            elif 'span' in line:
                pointer += 1
                temp = line.strip()
                arrindex = temp.index('>') + 1
                trrindex = temp.index('</span>')
                record[fields[pointer]] = temp[arrindex:trrindex]


agmarknet(sys.argv[1], sys.argv[2])
