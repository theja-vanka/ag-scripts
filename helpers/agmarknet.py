# Import helper modules
import os
import calendar
import logging
import time

# Import Selenium
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

# Set Environmental variables
# Set geckodriver path
os.environ["PATH"] += os.pathsep + '/home/vanka/Documents/models/prakshep/'

# Init Logging
logging.basicConfig(
    filename="agarrivals.log",
    format='%(asctime)s %(message)s',
    filemode='w'
)
logger = logging.getLogger()
logger.setLevel(logging.ERROR)


# Webdriver Options
options = Options()
options.headless = True
options.set_preference("browser.download.folderList", 2)
options.set_preference("browser.download.manager.showWhenStarting", False)
options.set_preference("browser.download.dir", "/home/vanka/Documents/models/prakshep/dfiles")
options.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/ms-excel")

# Init Webdriver
browser = webdriver.Firefox(options=options)
# Init Url
url = 'https://agmarknet.gov.in/'
browser.implicitly_wait(100)

# Helper Months
monthdict = {
    1: 'Jan',
    2: 'Feb',
    3: 'Mar',
    4: 'Apr',
    5: 'May',
    6: 'Jun',
    7: 'Jul',
    8: 'Aug',
    9: 'Sep',
    10: 'Oct',
    11: 'Nov',
    12: 'Dec'
}

# Loop for years
for year in range(2012, 2013, 1):
    # Loop for months
    for month in range(9, 13, 1):
        # Get days in corresponding month
        num_days = calendar.monthrange(year, month)[1]
        days = [
            f'{day}-{monthdict[month]}-{year}' for day in range(1, num_days+1)
        ]
        # Create folder for organizing
        dumppath = f'/home/vanka/Documents/models/prakshep/dfiles/Frame_{year}_{month}'
        os.mkdir(dumppath)
        for day in days:
            try:
                browser.get(url)
                browser.find_element_by_xpath(
                    "//select[@name='ddlArrivalPrice']/option[text()='Arrival']"
                ).click()
                browser.find_element_by_xpath(
                    "//select[@name='ddlCommodity']/option[text()='Potato']"
                ).click()
                browser.find_element_by_xpath(
                    "//input[@name='btnGo']"
                ).click()

                element = browser.find_element_by_xpath(
                    "//input[@name='ctl00$txtDate']"
                )
                browser.execute_script(
                    "arguments[0].setAttribute('value',arguments[1])",
                    element,
                    day
                )

                element = browser.find_element_by_xpath(
                    "//input[@name='ctl00$txtDateTo']"
                )
                browser.execute_script(
                    "arguments[0].setAttribute('value',arguments[1])",
                    element,
                    day
                )

                browser.find_element_by_xpath(
                    "//input[@name='ctl00$cphBody$ButtonExcel']"
                ).click()
                time.sleep(2)
                files = os.listdir('/home/vanka/Documents/models/prakshep/dfiles')
                for _ in files:
                    if _.endswith(".xls"):
                        os.renames(
                            f'/home/vanka/Documents/models/prakshep/dfiles/{_}',
                            f'{dumppath}/Arrival_{day}.xls'
                        )
            except Exception as e:
                logger.error(f'Did not work for: <{day}>')
                browser.get(url)
                print(e)
